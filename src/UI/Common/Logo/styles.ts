import { StyleSheet } from 'react-native';

import TYPOGRAPHIC from 'Constants/typographic';
import { FONT_COLORS } from 'Constants/colors';

const styles = StyleSheet.create({
    container: {
        display: 'flex',
        flexDirection: 'row'
    },
    logo: {
        marginLeft: 4,
        width: 42,
        height: 42
    },
    firstItem: {
        ...TYPOGRAPHIC.h1,
        color: FONT_COLORS.PRIMARY
    },
    secondItem: {
        ...TYPOGRAPHIC.h1,
        color: FONT_COLORS.SECONDARY,
        marginLeft: -6
    }
});

export default styles;
