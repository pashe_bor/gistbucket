import React from 'react';
import { Text, View } from 'react-native';

import LogoIcon from 'Assets/svg/logo/logo.svg';

import styles from './styles';

const Logo: React.FunctionComponent = () => (
    <View style={styles.container}>
        <Text style={styles.firstItem}>Glitch</Text>
        <LogoIcon
            height={styles.logo.width}
            width={styles.logo.height}
            viewBox="0 0 200 250"
            style={styles.logo}
        />
        <Text style={styles.secondItem}>Bucket</Text>
    </View>
);

export default Logo;
