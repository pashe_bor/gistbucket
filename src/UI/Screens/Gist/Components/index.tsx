import * as React from 'react';
import { Text } from 'react-native';

import { FONT_COLORS } from 'Constants/colors';

const GistScreen: React.FunctionComponent = () => (
    <Text style={{ color: FONT_COLORS.SECONDARY }}>Gist screen</Text>
);

export default GistScreen;
