import React from 'react';

import AppView from 'Decorators/AppView';

import GistScreen from 'Screens/Gist/Components';

@AppView
class GistScreenContainer extends React.Component {
    render(): React.ReactElement {
        return <GistScreen />;
    }
}

export default GistScreenContainer;
