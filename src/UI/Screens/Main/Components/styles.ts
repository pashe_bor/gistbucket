import { StyleSheet } from 'react-native';

import { FONT_COLORS, BG_COLORS } from 'Constants/colors';
import TYPOGRAPHIC from 'Constants/typographic';

const styles = StyleSheet.create({
    container: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'space-between',
        backgroundColor: BG_COLORS.PRIMARY,
        height: '90%'
    },
    title: {
        ...TYPOGRAPHIC.h1,
        textAlign: 'left',
        marginBottom: 16
    },
    list: {
        flex: 1,
        borderRadius: 5,
        borderWidth: 2,
        borderColor: BG_COLORS.SECONDARY
    },
    item: {
        borderBottomWidth: 1,
        paddingHorizontal: 10,
        paddingVertical: 5,
        borderBottomColor: FONT_COLORS.SECONDARY,
        color: FONT_COLORS.TITLE
    }
});

export default styles;
