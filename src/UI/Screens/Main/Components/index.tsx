import * as React from 'react';
import { Text, View, FlatList, SafeAreaView } from 'react-native';

import { EasyRouterNavigator } from 'react-native-easy-router';

import { Gist } from 'Api/Models/Gists';

import styles from './styles';

type Props = {
    navigator: EasyRouterNavigator;
    gists: Gist[] | null;
};

type DataType = { id: string; title: string };

const Item = ({ title, desc }: { title: string; desc: string }) => (
    <View style={styles.item}>
        <Text style={{ color: styles.item.color }}>{title}</Text>
        <Text style={{ color: styles.item.color }}>{desc}</Text>
    </View>
);

const MainScreen: React.FunctionComponent<Props> = ({ navigator, gists }) => {
    const renderItem = ({ item }: { item: Gist }) => {
        const gistName = Object.keys(item.files)[0];

        return <Item title={gistName} desc={item.description} />;
    };
    return (
        <View style={styles.container}>
            <Text style={styles.title}>Glitches list:</Text>
            <SafeAreaView style={styles.list}>
                <FlatList
                    data={gists}
                    keyExtractor={(item) => item.id}
                    renderItem={renderItem}
                />
            </SafeAreaView>
        </View>
    );
};

export default MainScreen;
