import * as React from 'react';
import { connect } from 'react-redux';

import GlitchBucketApi from 'Api/GlitchBucketApi';

import AppView from 'Decorators/AppView';

import MainScreen from 'Screens/Main/Components';

import { GistStateType } from 'AppStore/reducers/gistsReducer';
import { addGists } from 'AppStore/actions/gistsActions';
import { Gist } from 'Api/Models/Gists';

type Props = Pick<GistStateType, 'gists'> & {
    navigator: any;
    addGists: (payload: Gist[]) => void;
};

const TOKEN = '4a61ade2e6bc6869d10f4888d0973e1daae0c3c5';

const mapState = ({ gistsReducer }: { gistsReducer: GistStateType }) => ({
    gists: gistsReducer.gists
});

const mapDispatch = {
    addGists
};

@AppView
class MainScreenContainer extends React.Component<Props> {
    constructor(props: Props) {
        super(props);

        this.state = {
            gists: null
        };
    }
    async componentDidMount(): Promise<void> {
        const api = new GlitchBucketApi('https://api.github.com', TOKEN);

        const response = await api.gists.getUserGists();

        this.props.addGists(response);
    }

    render(): React.ReactElement {
        return <MainScreen {...this.props} />;
    }
}

export default connect(mapState, mapDispatch)(MainScreenContainer);
