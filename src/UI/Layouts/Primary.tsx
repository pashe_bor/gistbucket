import * as React from 'react';
import Navigator, { EasyRouterNavigator } from 'react-native-easy-router';
import { StatusBar, View } from 'react-native';

import { BG_COLORS } from 'Constants/colors';

import Header from 'Root/Header/Containers';
import MainScreen from 'Screens/Main/Containers';
import GistScreen from 'Screens/Gist/Containers';
import Menu from 'Root/Menu';

import styles from './styles';

type State = {
    navigator: EasyRouterNavigator | null;
};

class PrimaryLayout extends React.Component<{}, State> {
    state = {
        navigator: null
    };

    render() {
        const { navigator } = this.state;
        return (
            <>
                <StatusBar backgroundColor={BG_COLORS.PRIMARY} />
                <View style={styles.container}>
                    <Header />
                    <Navigator
                        screens={{ MainScreen, GistScreen }}
                        initialStack="MainScreen"
                        navigatorRef={(ref) => {
                            this.setState({ navigator: ref });
                        }}
                    />
                    <Menu navigator={navigator} />
                </View>
            </>
        );
    }
}
export default PrimaryLayout;
