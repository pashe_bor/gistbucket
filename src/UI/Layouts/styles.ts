import { StyleSheet, Dimensions } from 'react-native';

import { BG_COLORS, FONT_COLORS } from 'Constants/colors';

const styles = StyleSheet.create({
    container: {
        height: Dimensions.get('screen').height,
        backgroundColor: BG_COLORS.PRIMARY,
        color: FONT_COLORS.PRIMARY
    }
});

export default styles;
