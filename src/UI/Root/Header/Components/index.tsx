import React from 'react';
import { View } from 'react-native';

import Logo from 'Common/Logo';

import styles from './styles';

const Header: React.FunctionComponent = () => {
    const onPressHandler = (): void => {
        console.log('test');
    };

    return (
        <View style={styles.container}>
            <Logo />
        </View>
    );
};

export default Header;
