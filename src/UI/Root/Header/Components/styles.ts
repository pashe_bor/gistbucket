import { StyleSheet } from 'react-native';

import { BG_COLORS } from 'Constants/colors';

const styles = StyleSheet.create({
    container: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        backgroundColor: BG_COLORS.SECONDARY,
        paddingLeft: 15,
        paddingRight: 15,
        paddingVertical: 8
    }
});

export default styles;
