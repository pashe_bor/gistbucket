import React from 'react';

import Header from 'Root/Header/Components';

const HeaderContainer: React.FunctionComponent = () => <Header />;

export default HeaderContainer;
