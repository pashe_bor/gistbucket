import { StyleSheet } from 'react-native';

import { BG_COLORS } from 'Constants/colors';

const styles = StyleSheet.create({
    menuBtn: {
        position: 'absolute',
        bottom: '15%',
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        right: 15,
        paddingBottom: 3,
        width: 68,
        height: 68,
        borderRadius: 60,
        backgroundColor: BG_COLORS.SECONDARY,
        elevation: 5
    }
});

export default styles;
