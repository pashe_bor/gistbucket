import React from 'react';
import { View } from 'react-native';

import Hamburger from 'Root/Menu/Hamburger';

import { EasyRouterNavigator } from 'react-native-easy-router';

import styles from './styles';

type Props = {
    navigator?: EasyRouterNavigator | null;
};

const Menu: React.FunctionComponent<Props> = ({ navigator }) => (
    <View style={styles.menuBtn}>
        <Hamburger navigator={navigator} />
    </View>
);

export default React.memo(Menu);
