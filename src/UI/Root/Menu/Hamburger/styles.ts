import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    container: {
        width: 48,
        height: 48,
        display: 'flex',
        alignItems: 'center'
    }
});

export default styles;
