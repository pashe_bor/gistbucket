import React from 'react';
import { View } from 'react-native';

import styles from './styles';

const appView: <P extends object>(
    WrappedComponent: React.ComponentClass<P>
) => void = <P extends object>(WrappedComponent: React.ComponentClass<P>) =>
    class AppView extends React.Component<P> {
        render(): React.ReactElement {
            return (
                <View style={styles.container}>
                    <WrappedComponent {...this.props} />
                </View>
            );
        }
    };

export default appView;
