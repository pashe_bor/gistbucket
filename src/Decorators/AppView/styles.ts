import { Dimensions, StyleSheet } from 'react-native';

import { BG_COLORS } from 'Constants/colors';

const styles = StyleSheet.create({
    container: {
        backgroundColor: BG_COLORS.PRIMARY,
        height: Dimensions.get('window').height,
        padding: 15
    }
});

export default styles;
