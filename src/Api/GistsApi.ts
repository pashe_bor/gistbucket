import Transport from 'Utils/Transport';
import { Gist } from 'Api/Models/Gists';

class GistsApi {
    constructor(private transport: Transport) {}

    getUserGists() {
        return this.transport.fetchData<Gist[], Gist>('/gists', {}, Gist);
    }
}

export default GistsApi;
