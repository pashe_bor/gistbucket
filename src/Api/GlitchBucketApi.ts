import GistsApi from 'Api/GistsApi';
import Transport from 'Utils/Transport';

class GlitchBucketApi {
    gists: GistsApi;

    constructor(baseUrl: string, authToken: string) {
        const request = new Transport(baseUrl, authToken);

        this.gists = new GistsApi(request);
    }
}

export default GlitchBucketApi;
