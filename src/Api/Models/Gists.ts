import { Expose } from 'class-transformer';

export class Gist {
    @Expose() id = '';
    @Expose() url = '';
    @Expose() files = {
        fileName: '',
        type: '',
        language: ''
    };
    @Expose() description = '';
}
