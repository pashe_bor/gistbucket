import axios from 'axios';
import { plainToClass } from 'class-transformer';

import { AxiosInstance } from 'axios';

export interface TInterface {
    _http: AxiosInstance;
    fetchData: <T, RT = T>(
        uri: string,
        params: Record<string, any>,
        responseType?: new () => RT
    ) => Promise<T>;
}

class Transport implements TInterface {
    _http: AxiosInstance;

    constructor(apiUrl: string, authToken: string) {
        this._http = axios.create({
            baseURL: apiUrl,
            headers: {
                'Content-Type': 'application/json',
                Authorization: `token ${authToken}`
            }
        });
    }

    private formatResponse(response: any, responseType: any) {
        if (response) {
            try {
                const body =
                    typeof response === 'string'
                        ? JSON.parse(response)
                        : response;

                let result: typeof responseType;

                result = plainToClass(responseType, body, {
                    excludeExtraneousValues: true
                });
                return result;
            } catch (e) {
                throw new Error(e);
            }
        } else {
            return {};
        }
    }

    async fetchData<T, RT = T>(
        uri: string,
        params: Record<string, any>,
        responseType?: new () => RT
    ): Promise<T> {
        try {
            const response = await this._http.get(uri, {
                params
            });

            return this.formatResponse(response.data, responseType);
        } catch (error) {
            throw error;
        }
    }
}

export default Transport;
