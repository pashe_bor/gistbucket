export const BG_COLORS = Object.freeze({
    PRIMARY: '#0e141b',
    SECONDARY: '#32373d'
});

export const FONT_COLORS = Object.freeze({
    PRIMARY: '#ffffff',
    SECONDARY: '#eb2f96',
    TITLE: '#ffe100'
});

export const BTN_COLORS = Object.freeze({
    PRIMARY: '#eb2f96'
});
