const FONT_SIZES = Object.freeze({
    TITLE_EPIC: 36,
    TITLE_HUGE: 32,
    TITLE_BIG: 28,
    TITLE_MEDIUM: 24,
    TITLE_SUB: 20,
    TEXT_NORMAL: 16
});

export default FONT_SIZES;
