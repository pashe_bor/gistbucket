import { StyleSheet } from 'react-native';

import { FONT_COLORS } from 'Constants/colors';
import FONT_SIZES from 'Constants/fontSizes';

const TYPOGRAPHIC = StyleSheet.create({
    h1: {
        fontSize: FONT_SIZES.TITLE_EPIC,
        fontFamily: 'YanoneKaffeesatz-Bold',
        color: FONT_COLORS.TITLE
    },
    h2: {
        fontSize: FONT_SIZES.TITLE_HUGE,
        fontFamily: 'YanoneKaffeesatz-Bold',
        color: FONT_COLORS.TITLE
    },
    h3: {
        fontSize: FONT_SIZES.TITLE_BIG,
        fontFamily: 'YanoneKaffeesatz-SemiBold',
        color: FONT_COLORS.PRIMARY
    },
    h4: {
        fontSize: FONT_SIZES.TITLE_MEDIUM,
        fontFamily: 'YanoneKaffeesatz-SemiBold',
        color: FONT_COLORS.PRIMARY
    },
    h5: {
        fontSize: FONT_SIZES.TITLE_SUB,
        fontFamily: 'YanoneKaffeesatz-Medium',
        color: FONT_COLORS.PRIMARY
    },
    text: {
        fontSize: FONT_SIZES.TEXT_NORMAL,
        fontFamily: 'YanoneKaffeesatz-Medium',
        color: FONT_COLORS.PRIMARY
    }
});

export default TYPOGRAPHIC;
