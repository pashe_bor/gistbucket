export type ActionType<P> = { type: string; payload: P };
