import { Gist } from 'Api/Models/Gists';

import { ActionType } from 'AppStore/__types__';

export const ADD_GISTS = 'ADD_GISTS';

export const addGists = (payload: Gist[]): ActionType<Gist[]> => ({
    type: ADD_GISTS,
    payload
});
