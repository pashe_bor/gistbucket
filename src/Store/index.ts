import { createStore } from 'redux';

import rootReducer from 'AppStore/reducers';

export const store = createStore(rootReducer);

export default store;
