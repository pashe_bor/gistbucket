import { combineReducers } from 'redux';

import gistsReducer from 'AppStore/reducers/gistsReducer';

const rootReducer = combineReducers({
    gistsReducer
});

export default rootReducer;
