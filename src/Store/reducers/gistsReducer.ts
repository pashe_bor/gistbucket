import { Gist } from 'Api/Models/Gists';

import { ADD_GISTS } from 'AppStore/actions/gistsActions';

import { ActionType } from 'AppStore/__types__';

export type GistStateType = {
    gists: Gist[] | null;
};

const initialState: GistStateType = {
    gists: null
};

const gistsReducer = (
    state = { ...initialState },
    action: ActionType<Gist[]>
) => {
    switch (action.type) {
        case ADD_GISTS:
            return {
                ...state,
                gists: action.payload
            };
        default:
            return state;
    }
};

export default gistsReducer;
