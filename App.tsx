import React from 'react';
import { Provider } from 'react-redux';

import store from 'AppStore/index';

import PrimaryLayout from 'Layouts/Primary';

const App = () => (
    <Provider store={store}>
        <PrimaryLayout />
    </Provider>
);

export default App;
