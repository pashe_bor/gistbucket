module.exports = {
    presets: ['module:metro-react-native-babel-preset'],
    plugins: [
        ['@babel/plugin-proposal-decorators', { legacy: true }],
        [
            'module-resolver',
            {
                root: ['./src'],
                extensions: ['.tsx', '.ts'],
                alias: {
                    Layouts: './src/UI/Layouts',
                    Constants: './src/constants',
                    Screens: './src/UI/Screens',
                    Common: './src/UI/Common',
                    Root: './src/UI/Root',
                    Assets: './src/assets',
                    Decorators: './src/Decorators',
                    Hooks: './src/Hooks',
                    Api: './src/Api',
                    Utils: './src/utils',
                    AppStore: './src/Store'
                }
            }
        ]
    ]
};
